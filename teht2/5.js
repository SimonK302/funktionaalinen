'use strict';

let f, g;
var Moduuli = (function () {
  let _x = 1;
  return {
    f: function(){
        let _f = _x;
        return function (){
            return _f+=1;
        }
    }(),
    g: function(){
        let _g = _x;
        return function (){
            return _g-=1;
        }
    }()
  };
})();

console.log(Moduuli.f());
console.log(Moduuli.f());
console.log(Moduuli.g());