const f = function () {
    return function(x,y){
        if(x>y){
            return 1;
        }else if(x<y){
            return -1;
        }else return 0;
    }
}();

function comp(fraasi, v1, v2){
    let count = 0;
    for(var i=0; i<v1.length; i++){
        if (fraasi(v1[i], v2[i])===-1){
            count++;
        }
    }
    return count;
}

let v15 = [-0.9, -2.4, -1.8, 2.2, 10.8, 16.6, 20.1, 18.0, 9.9, 2.2, -5.5, -10.9];
let v16 =[-0.9, -2.8, -1.0, 3.5, 9.8, 18.6, 20.1, 18.9, 7.9, 6.2, 5.5, -11.9];

console.log(comp(f, v15,v16));