'use strict'

const offset = [1,2];
const zoom = 2;
const pi = Math.PI;

const point = { x: 1, y: 1};

const pipeline  = [   // 2D-muunnoksia
    
    function translate(p){
        return { x: p.x + offset[0], y: p.y + offset[1] };
    },

    function scale(p){
        return { x: p.x * zoom, y: p.y * zoom};
    },
    function rotate(p){
        //Ilman pyöristystä tulos heitti 0.000000000000001 yksikköä
        return {x: Math.round(p.x*Math.cos(pi)-p.y*Math.sin(pi)), y: Math.round(p.x*Math.sin(pi) + p.y*Math.cos(pi))}
    }
];


function muunnos(point){
     for(let i=0; i<pipeline.length; i++){   
        point = pipeline[i](point);
    }
    return point;
}


console.log(point);
console.log(muunnos(point));