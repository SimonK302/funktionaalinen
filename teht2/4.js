function pow(x, y){
    function powHelper(x2, y2, acc){
        if(y2===0){
            return acc;
        }else{
            return powHelper(x2, y2-1, acc*x2)
        }
    }
    return powHelper(x, y, 1);
}

console.log(pow(3, 4))