const f = function () {
    return function(x,y){
        if(x>y){
            return 1;
        }else if(x<y){
            return -1;
        }else return 0;
    }
}();

let tulos1 = f(1,2);
console.log(tulos1);

let tulos2 = f(2,1);
console.log(tulos2);

let tulos3 = f(1,1);
console.log(tulos3);