function onPalindromi(str){
    if(str.length === 0 || str.length === 1){
        return true;
    }else if(str[0] !== str[str.length-1]){
        return false;
    }else{
        return onPalindromi(str.substring(1, str.length-1));
    }
}

console.log(onPalindromi("otto"));
console.log(onPalindromi("pekka"));